/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Joshua Behrens
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 **/

#ifndef __LIBNIMNPP_SCHEMA_H
#define __LIBNIMNPP_SCHEMA_H

#ifndef __cplusplus
#error libnimn++ is a c++ only library
#endif // __cplusplus

#include <any>
#include <deque>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/indexed_by.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>

using namespace boost::multi_index;

namespace nimn {
    typedef std::pair<std::string, std::any> schema_key_value;

    typedef multi_index_container<
            schema_key_value,
            indexed_by<
                    random_access<>,
                    hashed_unique<member<schema_key_value, schema_key_value::first_type, &schema_key_value::first>>
            >
    > schema_object;

    typedef schema_object schema;
}

#endif // __LIBNIMNPP_SCHEMA_H
