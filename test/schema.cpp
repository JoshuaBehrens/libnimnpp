#include <catch.hpp>
#include <nimn/nimn.h>

TEST_CASE("Schema keeps order", "[schema][basic]") {
    nimn::schema schema;
    schema.push_back({ "5", std::string("value1") });
    schema.push_back({ "1", nimn::schema_object() });

    auto iterator = schema.get<0>().begin();
    REQUIRE(iterator->first == "5");
    REQUIRE((++iterator)->first == "1");
}

TEST_CASE("Schema does not allow insertion on duplicate key", "[schema][basic]") {
    nimn::schema schema;
    schema.push_back({ "5", std::string("value1") });
    schema.push_back({ "5", nimn::schema_object() });
    REQUIRE(schema.get<0>().size() == 1);
    REQUIRE(schema.get<0>().begin()->second.type() == typeid(std::string));
    REQUIRE(std::any_cast<std::string>(schema.get<0>().begin()->second) == "value1");
}
